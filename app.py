import os
import sys
from flask import Flask, request, redirect
from flask_mail import Mail, Message
# set the project root directory as the static folder, you can set others.
_current_dir = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__, static_url_path=r'', static_folder=_current_dir)
app.config.update(dict(
    MAIL_SERVER = 'smtp.googlemail.com',
    MAIL_PORT = 465,
    MAIL_USE_TLS = False,
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'creer.lettre@gmail.com',
    MAIL_PASSWORD = os.environ.get('MAIL_PASS', '')
))
mail = Mail(app)

@app.route('/')
def root():
    return app.send_static_file(r'index.html')

@app.route('/send_mail', methods=["POST"])
def send_mail():
    if not os.environ.get('MAIL_PASS', ''):
        return "Failed to send email"
    msg = Message("[LettreMotivation] From " + request.form["name"],
        sender=request.form["email"],
        recipients=os.environ.get('EMAILS', 'creer.lettre@gmail.com').split(","),
        body=request.form["message"])
    mail.send(msg)
    return redirect("/", code=302)


if __name__ == "__main__":
    app.run()
